const fs = require("fs");

const config = {
  // generated text from https://pdftotext.github.io/
  path: "./Chrysostomica_A Bibliography of Scholarship on John Chrysostom by Mayer.pdf.txt",
  encoding: "utf-8",
  outputPath: "./report.csv",
  delimeter: ",", // That can be changed to "|" for tab delimited, this would let you keep the ","
  recordSplit: "]",
  keyWords: ["paris", "chrysostom"], // case insensitive
  version: 2,
};

const longString = fs
  .readFileSync(config.path, config.encoding)
  .replace(config.delimeter);

const rawRecords = longString.split(config.recordSplit);

class Line {
  constructor(rawString, possibleDates) {
    this.rawString = rawString;
    this.possibleDates = possibleDates;
  }
  static getHeaders() {
    return (
      [
        "rawString",
        "possibleDates",
        "containsSearchTerm",
        "containsAllSearchTerms",
      ].join(config.delimeter) + "\n"
    );
  }
  get() {
    const text = this.rawString.split(config.delimeter).join("").trim();
    const textLower = text.toLowerCase();
    let allKeyWordsFound = true;
    let keyWordFound = null;
    for (const keyWord of config.keyWords) {
      if (textLower.includes(keyWord)) keyWordFound = true;
      else allKeyWordsFound = false;
    }

    return (
      [text, this.possibleDates, keyWordFound, allKeyWordsFound].join(
        config.delimeter
      ) + "\n"
    );
  }
}

const output = [];
const dateRegEx = /[0-9]{4}/g;
for (const record of rawRecords) {
  let rawDates = record.match(dateRegEx);
  let sortedDates = null;
  if (rawDates) {
    sortedDates = rawDates.sort().join(" ");
  }
  output.push(new Line(record.trim(), sortedDates));
}

// Writing the output
const stream = fs.createWriteStream(config.outputPath, { flags: "w" });
stream.write(Line.getHeaders());
for (const record of output) {
  stream.write(record.get());
}
stream.end();
